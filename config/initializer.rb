ROOT = File.dirname(File.dirname(__FILE__))
$: << ROOT

require 'yaml'
require 'date'
require 'openssl'
require 'active_record'

CONFIG = YAML.load(
  File.read(File.join(ROOT, 'config','config.yml'))
)

ActiveRecord::Base.establish_connection(CONFIG['database'])
