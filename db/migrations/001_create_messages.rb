
class CreateMessages < ActiveRecord::Migration[4.2]
  def up
    create_table :messages do |t|
      t.string :queue_id
      t.string :message_id
      t.datetime :first_seen_at
      t.datetime :sent_at
      t.datetime :received_at
      t.datetime :date
      t.string :sender
      t.string :recipient
      t.string :orig_to
      t.string :from
      t.text :to
      t.text :cc
      t.text :bcc
      t.integer :size
      t.integer :spam_score
      t.integer :subject_length
      t.boolean :is_list, :default => false
      t.boolean :is_outgoing
      t.string :re_message_id
      t.string :status
      t.float :delay
      t.string :delays
      t.boolean :tor
    end
    add_index :messages, :queue_id
  end

  def down
    drop_table :messages
  end
end

